from django.shortcuts import render

# Create your views here.

def admin_base(request):
    return render(request, "admin_base.html")

def dashboard(request):
    return render(request, "admin_panel/dashboard.html")

def openorder(request):
    return render(request, "admin_panel/openorder.html")

def orderhistory(request):
    return render(request, "admin_panel/order_history.html")

def tradehistory(request):
    return render(request, "admin_panel/trade_history.html")

def runningTrade(request):
    return render(request, "admin_panel/running_trade.html")

def reportedTrade(request):
    return render(request, "admin_panel/reported_trade.html")

def completedTrade(request):
    return render(request, "admin_panel/completed_trade.html")

def manageAd(request):
    return render(request, "admin_panel/manage_ad.html")

def paymentWindow(request):
    return render(request, "admin_panel/payment_window.html")

def paymentMethod(request):
    return render(request, "admin_panel/payment_method.html")


def cryptoCurrency(request):
    return render(request, "admin_panel/crypto_currency.html")

def flatCurrency(request):
    return render(request, "admin_panel/flat_currency.html")


def manageMarket(request):
    return render(request, "admin_panel/manage_market.html")

def manageCoinPair(request):
    return render(request, "admin_panel/manage_coinpair.html")

def activeUsers(request):
    return render(request, "admin_panel/active_users.html")

def bannedUsers(request):
    return render(request, "admin_panel/banned_users.html")

def emailUnverified(request):
    return render(request, "admin_panel/email_unverified.html")

def mobileUnverified(request):
    return render(request, "admin_panel/mobile_unverified.html")

def kycUnverified(request):
    return render(request, "admin_panel/kyc_unverified.html")

def kycPending(request):
    return render(request, "admin_panel/kyc_pending.html")

def allUsers(request):
    return render(request, "admin_panel/all_users.html")

def notifyToAll(request):
    return render(request, "admin_panel/notify_to_all.html")


def manageReferrals(request):
    return render(request, "admin_panel/manage_referrals.html")

def automaticGateways(request):
    return render(request, "admin_panel/automatic_gateways.html")

def manualGateways(request):
    return render(request, "admin_panel/manual_gateways.html")

def pendingDeposits(request):
    return render(request, "admin_panel/pending_Deposits.html")

def approvedDeposits(request):
    return render(request, "admin_panel/approved_Deposits.html")

def successfulDeposits(request):
    return render(request, "admin_panel/successful_Deposits.html")

def rejectedDeposits(request):
    return render(request, "admin_panel/rejected_Deposits.html")

def initiatedDeposits(request):
    return render(request, "admin_panel/initiated_Deposits.html")

def allDeposits(request):
    return render(request, "admin_panel/all_Deposits.html")

def withdrawlMethods(request):
    return render(request, "admin_panel/withdrawl_methods.html")

def pendingWithdrawls(request):
    return render(request, "admin_panel/pending_withdrawls.html")

def approvedWithdrawls(request):
    return render(request, "admin_panel/approved_withdrawls.html")

def rejectedWithdrawls(request):
    return render(request, "admin_panel/rejected_withdrawls.html")

def allWithdrawls(request):
    return render(request, "admin_panel/all_withdrawls.html")

def pendingTicket(request):
    return render(request, "admin_panel/pending_ticket.html")

def closedTicket(request):
    return render(request, "admin_panel/closed_ticket.html")

def answeredTicket(request):
    return render(request, "admin_panel/answered_ticket.html")

def allTicket(request):
    return render(request, "admin_panel/all_ticket.html")

def transactionLog(request):
    return render(request, "admin_panel/transaction_log.html")

def loginHistory(request):
    return render(request, "admin_panel/login_history.html")

def notificationHistory(request):
    return render(request, "admin_panel/notification_history.html")